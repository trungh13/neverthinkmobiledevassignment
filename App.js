import React, {useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  Text,
  Dimensions,
} from 'react-native';

import YoutubePlayer from './Components/YoutubePlayer';
import Channels from './Components/Channels';

import Colors from './Colors';

const App = () => {
  const channels = require('./dummyChannels.json');

  const [safeAreaWidth, setSafeAreaWidth] = useState(0);
  const [curChannel, setCurChannel] = useState(channels[1]);
  const [videoState, setVideoState] = useState({
    videoIdList: curChannel.playlist,
    isReady: false,
    status: '',
    quality: false,
    error: '',
    playerWidth: safeAreaWidth || Dimensions.get('window').width,
  });

  return (
    <SafeAreaView style={styles.safeAreaView}>
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={styles.scrollView}
        onLayout={event => {
          setSafeAreaWidth(event.nativeEvent.layout.width);
          setVideoState({
            ...videoState,
            playerWidth: safeAreaWidth,
          });
        }}>
        <YoutubePlayer
          videoState={videoState}
          setVideoState={setVideoState}
          safeAreaWidth={safeAreaWidth}
        />
        <Channels
          channels={channels}
          safeAreaWidth={safeAreaWidth}
          curChannel={curChannel}
          setCurChannel={setCurChannel}
          setVideoState={setVideoState}
          videoState={videoState}
        />
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeAreaView: {
    flex: 1,
    backgroundColor: Colors.defaultBg,
  },
});

export default App;
