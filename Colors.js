const Colors = {
  appTextColor: 'rgba(255, 255, 255, 0.9)',
  defaultBg: '#0f0f0f',
  channelBg: '#1f1f1f',
  selectedChannelBorderColor: '#ffb400',
};
export default Colors;
