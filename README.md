# Neverthink Assignment

This project was bootstrapped with [Create React Native App](https://github.com/react-community/create-react-native-app).

Below you'll find information about performing common tasks. The most recent version of this guide is available [here](https://github.com/react-community/create-react-native-app/blob/master/react-native-scripts/template/README.md).

## Installation

Clone the project from the [repo](https://gitlab.com/trungh13/neverthinkmobiledevassignment) in Gitlab.

```
git clone https://gitlab.com/trungh13/neverthinkmobiledevassignment.git
```

Install the node modules package.

```
yarn
```

Install Pod packages for iOS and back to root folder

```
cd ios/
pod install
cd ..
```

## Available Scripts

If Yarn was installed when the project was initialized, then dependencies will have been installed via Yarn, and you should probably use it to run these commands as well. Unlike dependency installation, command running syntax is identical for Yarn and NPM at the time of this writing.

### `yarn run ios`

Make iOS build and runs your app with iOS emulator(Mac only) in development mode.

If you have the problem `No bundle URL present`, please run this script

```
rm -rf ios/build/
kill $(lsof -t -i:8081)
yarn run ios
```

Remove the `ios/build/` folder, kill the process at port `8081`(which default port of React native app). Make new iOS build for emulator.

## Deploy to device

For deployment to device you can take the look at the [React native document](https://facebook.github.io/react-native/docs/running-on-device)
