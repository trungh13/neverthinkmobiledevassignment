import React from 'react';
import {StyleSheet, View, Text} from 'react-native';

import Channel from './Channel';

import Colors from '../Colors';

const Channels = ({
  channels,
  safeAreaWidth,
  curChannel,
  setCurChannel,
  setVideoState,
  videoState,
}) => {
  return (
    <>
      <Text style={styles.channelHeader}>Channels</Text>
      <View style={styles.channels}>
        {channels.map(channel => (
          <Channel
            key={channel.id}
            safeAreaWidth={safeAreaWidth}
            channel={channel}
            curChannel={curChannel}
            setCurChannel={setCurChannel}
            videoState={videoState}
            setVideoState={setVideoState}
          />
        ))}
      </View>
    </>
  );
};
const styles = StyleSheet.create({
  channelHeader: {
    flex: 1,
    fontSize: 20,
    width: '100%',
    textAlign: 'center',
    fontWeight: 'bold',
    color: Colors.appTextColor,
    marginVertical: 5,
  },
  channels: {
    flex: 1,
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
export default Channels;
