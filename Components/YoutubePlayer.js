import React, {useState} from 'react';
import {StyleSheet, Dimensions, View, PixelRatio, Text} from 'react-native';
import YouTube from 'react-native-youtube';
const YoutubePlayer = ({videoState, setVideoState, safeAreaWidth}) => {
  const _youTubeRef = React.createRef();
  const RatioHeight169 = {
    height: Math.min(
      PixelRatio.roundToNearestPixel(videoState.playerWidth / (16 / 9)),
      Dimensions.get('window').height,
    ),
  };
  const loading =
    videoState.isReady &&
    ['playing', 'buffering', 'paused'].indexOf(videoState.status) === -1;
  const loadingMessage = 'Loading...';
  const notAvailableMessage = 'Video unavailabe';
  const [videoStatus, setVideoStatus] = useState(loadingMessage);
  const videoStatusTimer = () =>
    setTimeout(() => {
      setVideoStatus(notAvailableMessage);
    }, 1000);
  if (loading && videoStatus === loadingMessage) {
    videoStatusTimer();
  } else if (!loading && videoStatus === notAvailableMessage) {
    clearTimeout(videoStatusTimer);
    setVideoStatus(loadingMessage);
  }
  return (
    <>
      {(loading || !videoState.isReady) && (
        <View
          style={[RatioHeight169, styles.loadingView, {width: safeAreaWidth}]}>
          {loading && <Text style={styles.loadingText}>{videoStatus}</Text>}
          {!videoState.isReady && (
            <Text style={styles.loadingText}>Loading video player.</Text>
          )}
        </View>
      )}
      <YouTube
        ref={_youTubeRef}
        videoId={videoState.videoIdList[0]}
        play={true}
        loop={true}
        controls={1}
        style={[RatioHeight169, styles.youtubePlayer]}
        onError={e => {
          setVideoState({...videoState, error: e.error});
          if (_youTubeRef.current) {
            _youTubeRef.current.nextVideo();
          }
        }}
        onReady={e => {
          setVideoState({...videoState, isReady: true});
        }}
        onChangeState={e => {
          if (e.state === 'unstarted') {
            setVideoState({
              ...videoState,
              videoIdList: videoState.videoIdList.push(
                videoState.videoIdList.shift(),
              ),
            });
          }
          setVideoState({...videoState, status: e.state});
        }}
      />
    </>
  );
};
const styles = StyleSheet.create({
  youtubePlayer: {
    marginVertical: 10,
  },
  loadingView: {
    backgroundColor: 'black',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    zIndex: 999,
    marginVertical: 10,
  },
  loadingText: {
    color: 'white',
  },
});
export default YoutubePlayer;
