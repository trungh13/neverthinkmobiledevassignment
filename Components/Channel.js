import React from 'react';
import {StyleSheet, TouchableHighlight, Image, View, Text} from 'react-native';

import Colors from '../Colors';

const marginHorizontal = 5;

const Channel = ({
  safeAreaWidth,
  channel,
  curChannel,
  setCurChannel,
  videoState,
  setVideoState,
}) => {
  const channelsPerRow = 3;
  const viewStyles = [
    styles.channel,
    {
      width: safeAreaWidth / channelsPerRow - marginHorizontal * 2 || 0,
    },
  ];
  if (channel.id === curChannel.id) {
    viewStyles.push(styles.selectedChannel);
  }

  return (
    <TouchableHighlight
      key={channel.id}
      onPress={() => {
        setCurChannel(channel);
        setVideoState({
          ...videoState,
          videoIdList: channel.playlist,
        });
      }}>
      <View style={viewStyles}>
        <Image style={styles.channelIcon} source={{uri: channel.icon}} />
        <Text style={styles.channelName}>{channel.name}</Text>
      </View>
    </TouchableHighlight>
  );
};

export default Channel;

const styles = StyleSheet.create({
  channel: {
    flex: 1,
    flexDirection: 'column',
    paddingTop: 10,
    paddingBottom: 5,
    paddingHorizontal: 10,
    marginVertical: 10,
    marginHorizontal,
    backgroundColor: Colors.channelBg,
  },
  selectedChannel: {
    borderColor: Colors.selectedChannelBorderColor,
    borderStyle: 'solid',
    borderWidth: 2,
    paddingTop: 8,
    paddingBottom: 3,
  },
  channelIcon: {width: 30, height: 30},
  channelName: {
    color: Colors.appTextColor,
    fontWeight: 'bold',
    lineHeight: 30,
  },
});
